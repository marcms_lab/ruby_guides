#!/bin/bash

RUBY_VERSION="3.1.1"
SHELL="/bin/bash"
BREW_PATH="/home/linuxbrew/.linuxbrew/bin"
BREW_CMD="$BREW_PATH/brew"

# ---------- #

# Abort function
abort() {
  printf "\n[Error] %s\n" "$@" 1>&2
  exit 1
}

# Info function
info() {
  printf "\n[Info] %s\n" "$@"
}

# Format function
format() {
  printf "\n%s\n" "$@"
}

# ---------- #

# Update and upgrade
info "Updating the system..."
sudo apt-get update || abort "Error updating the system."

info "Upgrading the system..."
sudo apt-get upgrade -y || abort "Error upgrading the system."

# ---------- #

# Install basic packages
info "Installing basic packages..."
sudo apt-get install -y build-essential procps curl file zlib1g-dev git || abort "Error installing basic packages."

# ---------- #

# Install Homebrew
if ! [[ -x "$(command -v brew)" ]]; then
  info "Installing homebrew..."
  NONINTERACTIVE=1 $SHELL -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
  eval "$($BREW_CMD shellenv)"
fi

# Set homebrew into path
if ! [[ "$PATH" == *"$BREW_CMD"* ]]; then
  info "Setting homebrew into path..."
  export PATH="$BREW_CMD:$PATH"
  format "PATH ==> $PATH"
fi

# Persist homebrew into profile
if ! [[ -n "$(grep "$BREW_CMD" "$HOME/.profile")" ]]; then
  info "Persisting homebrew into profile..."
  format "eval \"\$($BREW_CMD shellenv)\""  >> $HOME/.profile
  format "export PATH=\"$BREW_CMD:\$PATH\"" >> $HOME/.profile
  tail "$HOME/.profile"
fi

# Check Homebrew installation
[[ -x "$(command -v brew)" ]] || eval "$($BREW_CMD shellenv)" || abort "Homebrew installation not found."

# ---------- #

# Install gcc
if ! [[ -x "$(command -v gcc)" ]]; then
  info "Installing gcc..."
  brew install gcc || abort "Error installing gcc."
fi

# Check gcc installation
[[ -x "$(command -v gcc)" ]] || abort "Gcc installation not found."

# ---------- #

# Install rbenv
if ! [[ -x "$(command -v rbenv)" ]]; then
  info "Installing rvenv..."
  brew install rbenv || abort "Error installing rvenv and ruby-build."
fi

# Set up rbenv into path
if ! [[ "$PATH" == *"$HOME/.rbenv/shims"* ]]; then
  info "Setting up rvenv into path..."
  eval "$(rbenv init - shellenv)"
  export PATH="$HOME/.rbenv/shims:$PATH"
  format "PATH=$PATH"
fi

# Persist rbenv into profile
if ! [[ -n "$(grep "$HOME/.rbenv/shims" "$HOME/.profile")" ]]; then
  info "Persisting rbenv into profile..."
  format "eval \"\$(rbenv init - shellenv)\""        >> $HOME/.profile
  format "export PATH=\"$HOME/.rbenv/shims:\$PATH\"" >> $HOME/.profile
  tail "$HOME/.profile"
fi

# Check rbenv installation
[[ -x "$(command -v rbenv)" ]] || eval "$(rbenv init - shellenv)" || abort "Rbenv installation not found."
rbenv rehash

# ---------- #

# Install ruby-build
if ! [[ -x "$(command -v ruby-build)" ]]; then
  info "Installing ruby-build..."
  brew install ruby-build || abort "Error installing rvenv and ruby-build."
fi

# Set up ruby-build into path
if ! [[ "$PATH" == *"ruby-build"* ]]; then
  info "Setting up ruby-build into path..."
  export PATH="$HOME/.rbenv/plugins/ruby-build/bin:$PATH"
  format "PATH=$PATH"
fi

# Persist ruby-build into profile
if ! [[ -n "$(grep "ruby-build" "$HOME/.profile")" ]]; then
  info "Persisting ruby-build into profile..."
  format "export PATH=\"$HOME/.rbenv/plugins/ruby-build/bin:\$PATH\"" >> $HOME/.profile
  tail "$HOME/.profile"
fi

# Check ruby-build installation
[[ -x "$(command -v ruby-build)" ]] || abort "Ruby-build installation not found."
rbenv rehash

# ---------- #

# List latest Ruby stable versions:
# rbenv install -l

# List all Ruby local versions:
# rbenv install -L

# Install ruby
if [ ! -d "$HOME/.rbenv/versions/$RUBY_VERSION" ]; then
  info "Installing Ruby $RUBY_VERSION..."
  rbenv install "$RUBY_VERSION" || abort "Error installing Ruby $RUBY_VERSION."
fi

# Set up ruby
if [[ -x "$(command -v ruby)" ]] && ! [[ "$(command -v ruby)" == *"$RUBY_VERSION"* ]]; then
  info "Setting up Ruby $RUBY_VERSION..."
  rbenv local $RUBY_VERSION
  rbenv shell $RUBY_VERSION
  rbenv rehash
fi

# Persist ruby into profile
if ! [[ -n "$(grep "rbenv shell $RUBY_VERSION" "$HOME/.profile")" ]]; then
  info "Persisting ruby shell $RUBY_VERSION into profile..."
  format "rbenv shell $RUBY_VERSION" >> $HOME/.profile
  tail $HOME/.profile
fi

# Check ruby installation
[[ -x "$(command -v ruby)" ]] && [[ "$(ruby -v)" == *"$RUBY_VERSION"* ]] || abort "Ruby $RUBY_VERSION installation not found."

# ---------- #

# Verify rbenv and ruby installation
info "Verifying rbenv and ruby installation..."
$SHELL -c "$(curl -fsSL https://github.com/rbenv/rbenv-installer/raw/main/bin/rbenv-doctor)"

# ---------- #

# Install sqlite3
if ! [[ -x "$(command -v sqlite3)" ]]; then
  info "Installing sqlite3..."
  brew install sqlite3 || abort "Error installing sqlite3."
fi

# Check sqlite3 installation
[[ -x "$(command -v sqlite3)" ]] || abort "Sqlite3 installation not found."

# ---------- #

# Install node
if ! [[ -x "$(command -v node)" ]]; then
  info "Installing node..."
  brew install node || abort "Error installing node."
fi

# Check node installation
[[ -x "$(command -v node)" ]] || abort "Node installation not found."

# ---------- #

# Install yarn
if ! [[ -x "$(command -v yarn)" ]]; then
  info "Installing yarn..."
  brew install yarn || abort "Error installing yarn."
fi

# Check yarn installation
[[ -x "$(command -v yarn)" ]] || abort "Yarn installation not found."

# ---------- #

# Install rails
if ! [[ -x "$(command -v rails)" ]]; then
  info "Installing rails..."
  gem install rails || abort "Error installing rails."
  rbenv rehash
fi

# Check rails installation
[[ -x "$(command -v rails)" ]] || abort "Rails installation not found."
