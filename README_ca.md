Guies de Ruby on Rails (v7.0.2.3)
====================

Aquestes són les noves guies per a Rails 7.0 basades en la [v7.0.2.3](https://github.com/rails/rails/tree/v7.0.2.3).
Aquestes guies estan dissenyades per fer que siguis productiu immediatament amb Rails i per ajudar-te a entendre com encaixen totes les peces.

Les guies de versions anteriors: [Rails 6.1](https://guides.rubyonrails.org/v6.1/), [Rails 6.0](https://guides.rubyonrails.org/v6.0/), [Rails 5.2](https://guides.rubyonrails.org/v5.2/), [Rails 5.1](https://guides.rubyonrails.org/v5.1/), [Rails 5.0](https://guides.rubyonrails.org/v5.0/), [Rails 4.2](https://guides.rubyonrails.org/v4.2/), [Rails 4.1](https://guides.rubyonrails.org/v4.1/), [Rails 4.0](https://guides.rubyonrails.org/v4.0/), [Rails 3.2](https://guides.rubyonrails.org/v3.2/), [Rails 3.1](https://guides.rubyonrails.org/v3.1/), [Rails 3.0 .3](https://guides.rubyonrails.org/v3.0/) i [Rails 2.3](https://guides.rubyonrails.org/v2.3/).

---

# Comença aquí

## [Primers passos amb Rails](0.start_here/0.1.getting_started_with_rails/README_ca.md)

Tot el que necessites saber per instal·lar Rails i crear la teva primera aplicació.

---

# Models

## [Conceptes bàsics d'_Active Record_](1.models/1.1.active_record_basics/README_ca.md)

> :warning: &nbsp; En desenvolupament

Active Record permet als vostres models interactuar amb la base de dades de l'aplicació. Aquesta guia us ajudarà a començar amb els models Active Record i la persistència a la base de dades.

---
