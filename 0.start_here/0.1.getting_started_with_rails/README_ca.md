
__Primers passos amb Rails__
====================

Aquesta guia ajuda a començar a utilitzar Ruby on Rails.

Després de llegir aquesta guia, sabràs:

:white_check_mark: &nbsp; **Com instal·lar Rails, crear una nova aplicació i connectar-la a una base de dades.**

:white_check_mark: &nbsp; **El disseny general d'una aplicació de Rails.**

:white_check_mark: &nbsp; **Els principis bàsics del disseny MVC (Model, Vista, Controlador) i del disseny RESTful.**

:white_check_mark: &nbsp; **Com generar ràpidament les peces inicials d'una aplicació de Rails.**



<br/>

## Índex

[[_TOC_]]

<br/>



## 1. Fonaments

Aquesta guia està dissenyada per als principiants que volen començar a crear una aplicació de Rails des de zero. No suposa que tingueu cap experiència prèvia amb Rails.

Rails és un framework d'aplicacions web que s'executa amb el llenguatge de programació Ruby. Si no teniu cap experiència prèvia amb Ruby, trobareu una corba d'aprenentatge molt pronunciada utilitzant directament Rails. Hi ha diverses llistes de recursos en línia per aprendre Ruby:

* [Pàgina web oficial del llenguatge de programació de Ruby](https://www.ruby-lang.org/en/documentation/)
* [Llistat de llibres de programació gratuïts](https://github.com/EbookFoundation/free-programming-books/blob/master/books/free-programming-books-langs.md#ruby)

Tingueu en compte que alguns recursos, tot i que són excel·lents, cobreixen versions anteriors de Ruby i és possible que no incloguin alguna sintaxi que veureu en el desenvolupament diari de Rails.

<br/>



## 2. Què és Rails?

Rails és un framework de desenvolupament d'aplicacions web escrit amb el llenguatge de programació Ruby. Està dissenyat per facilitar la programació d'aplicacions web fent suposicions sobre què és el que necessita cada desenvolupador per començar a programar. Et permet escriure menys codi per aconseguir més resultats que molts altres llenguatges i frameworks. Els programadors experimentats de Rails també informen que fa que el desenvolupament d'aplicacions web sigui més divertit.

Rails és un programari que es basa en diferents criteris. Assumeix que hi ha una "_millor_" manera de fer les coses i està dissenyat per fomentar-la i, en alguns casos, per desanimar les alternatives. Si apreneu "_The Rails Way_" probablement aconseguireu un augment enorme de la vostra productivitat. Si persistiu en mantenir hàbits antics d'altres llenguatges al vostre desenvolupament de Rails i intenteu utilitzar patrons que heu après en altres llocs, és possible que tingueu una experiència menys feliç.

La filosofia Rails inclou dos principis basics:

* __No et repeteixis:__ DRY (_Don't Repeat Yourself_) és un principi de desenvolupament de programari que estableix que "_Cada peça del coneixement ha de tenir una representació única, inequívoca i autoritzada dins d'un sistema". En no escriure la mateixa informació una i altra vegada, el nostre codi és més fàcil de mantenir, més extensible i produeix menys errors.

* __Convenció abans que configuració:__ Rails es basa en diferents criteris sobre la millor manera de fer moltes coses en una aplicació web i per defecte prefereix que els desenvolupadors seguiu un conjunt de convencions, en lloc d'exigir que especifiqueu minuciositats a través d'interminables fitxers de configuració.

<br/>



## 3. Crear un nou projecte

La millor manera de llegir aquesta guia és seguir-la pas a pas. Tots els passos són essencials per executar aquesta aplicació d'exemple i no calen codis ni passos addicionals.

Seguint aquesta guia, creareu un projecte de Rails anomenat Blog, un weblog (molt) senzill. Abans de començar a crear l'aplicació, heu d'assegurar-vos que teniu el mateix Rails instal·lat.

> :memo: &nbsp; Els exemples següents utilitzen `$` per representar la vostra comanda en la terminal d'un sistema operatiu semblant a UNIX, tot i que és possible que s'hagi personalitzat perquè aparegui de forma diferent. Si utilitzeu Windows, el vostre missatge serà semblant a `C:\source_code>`.

<br/>



### 3.1. Instal·lar Rails

Abans d'instal·lar Rails, hauríeu de comprovar que el vostre sistema té instal·lats els requisits previs adequats. Això inclou:

* Ruby
* SQLite3
* Node.js
* Yarn

<br/>



#### 3.1.1. Instal·lar Ruby

Obriu una nova terminal i accediu a línia de comandes. A macOS obriu Terminal.app; a Windows, trieu "Executar" al menú Inici i escriviu `cmd.exe`. Qualsevol comanda precedida amb un signe de dòlar `$` s'ha d'executar a la línia de comandes. Comproveu que teniu instal·lada una versió actual de Ruby:

```bash
$ ruby --version
ruby 2.7.0
```

Rails requereix la versió 2.7.0 o posterior de Ruby. És preferible utilitzar la darrera versió de Ruby. Si el número de versió retornat és inferior a aquest número (com ara 2.3.7 o 1.8.7), haureu d'instal·lar una versió nova de Ruby.

Per instal·lar Rails a Windows, primer haureu d'instal·lar el [Ruby Installer](https://rubyinstaller.org/).

Per obtenir més mètodes d'instal·lació per a la resta de sistemes operatius, feu una ullada a [ruby-lang.org](https://www.ruby-lang.org/en/documentation/installation/).

<br/>



#### 3.1.2. Instal·lar SQLite3

També necessitareu una instal·lació de la base de dades SQLite3. Molts sistemes operatius populars semblants a UNIX ja es publiquen amb una versió actual de SQLite3. Si no la teniu correctament instal·lada podeu trobar les instruccions d'instal·lació a la [pàgina web de SQLite3](https://www.sqlite.org/).

Comproveu que l'aplicació estigui instal·lada correctament i carregada en el vostre `PATH`:

```bash
$ sqlite3 --version
```

Aquesta comanda hauria de mostrar la seva versió.

<br/>



#### 3.1.3. Instal·lar Node.js i Yarn

Finalment, necessitareu tenir instal·lats Node.js i Yarn per a poder gestionar el JavaScript de la vostra aplicació.

Trobareu les instruccions d'instal·lació a la [pàgina web de Node.js](https://nodejs.org/en/download/). Comproveu que estigui instal·lat correctament amb la comanda següent:

```bash
$ node --version
```

s'ha de mostrar la versió del vostre Node.js. Assegureu-vos que sigui superior a 8.16.0.

Per instal·lar Yarn, seguiu les instruccions d'instal·lació de la [pàgina web de Yarn](https://classic.yarnpkg.com/en/docs/install).

L'execució d'aquesta comanda hauria de mostrar la versió de Yarn:

```bash
$ yarn --version
```

Si diu alguna cosa semblant a "1.22.0", Yarn s'ha instal·lat correctament.

<br/>



#### 3.1.4. Instal·lar Rails
Per instal·lar Rails, utilitzeu la comanda `gem install` proporcionada per RubyGems:

```bash
$ gem install rails
```

Per verificar que ho teniu tot instal·lat correctament, hauríeu de poder executar la següent comanda a la terminal:

```bash
$ rails --version
```

Si diu alguna cosa semblant a "Rails 7.0.0", ja esteu preparats per continuar.

<br/>



### 3.2. Crear l'aplicació Blog

Rails inclou una sèrie de scripts anomenats _generadors_ que estan dissenyats per facilitar-vos la vida de desenvolupador, creant tot el necessari per començar a treballar en una tasca concreta. Un d'ells és el nou generador d'aplicacions, que us proporcionarà la base d'una nova aplicació de Rails perquè no la hàgiu de crear vosaltres mateixos.

Per utilitzar aquest generador, obriu una terminal, navegueu a un directori on tingueu drets per crear fitxers i executeu:

```bash
$ rails new blog
```

Això crearà una aplicació de Rails anomenada Blog en un directori `blog` i instal·larà les gemmes que s'han especificat en el fitxer `Gemfile` utilitzant l'acció `bundle install`.

> :information_source: &nbsp; Podeu veure totes les opcions que accepta el generador d'aplicacions de Rails executant la comanda `rails new --help`.

Després de crear l'aplicació del Blog, navegueu a la seva carpeta:

```bash
$ cd blog
```

El directori `blog` tindrà una sèrie de fitxers i carpetes generades automàticament que formen l'estructura d'una aplicació de Rails. La major part de la feina d'aquest tutorial es farà a la carpeta `app`, però aquí teniu un resum bàsic de la funció de cadascun dels fitxers i carpetes que Rails crea per defecte:

| Carpeta d'arxius     | Propòsit |
| ---                  | ---      |
| app/                 | Conté els controladors, models, vistes, _helpers_, _mailers_, _channels_, _jobs_ i _assets_ per a la vostra aplicació. Us centrareu en aquesta carpeta durant la resta d'aquesta guia. |
| bin/                 | Conté l'script de `rails` que inicia la vostra aplicació i pot contenir altres scripts que feu servir per configurar, actualitzar, desplegar o executar la vostra aplicació. |
| config/              | Conté la configuració de les rutes de la vostra aplicació, la base de dades i molt més. Això es tracta amb més detall a [_Configuring Rails Applications_](https://guides.rubyonrails.org/v7.0/configuring.html). |
| config.ru            | Configuració del Rak per a servidors _Rak-based_ utilitzada per iniciar l'aplicació. Per obtenir més informació sobre Rack, consulteu la [pàgina web de Rack](https://rack.github.io/). |
| db/                  | Conté el vostre esquema de la base de dades actual, així com les diferents migracions de la base de dades. |
| Gemfile Gemfile.lock | Aquests fitxers us permeten especificar quines gemmes es necessiten per a la vostra aplicació de Rails. Aquests fitxers són utilitzats per la gema Bundler. Per obtenir més informació sobre Bundler, consulteu la [pàgina web de Bundler](https://bundler.io/). |
| lib/                 | Mòduls d'ampliació per a la vostra aplicació. |
| log/                 | Fitxers de log de l'aplicació. |
| public/              | Conté fitxers estàtics i _assets_ compilats. Quan la vostra aplicació s'executa, aquest directori s'exposarà a internet tal com està. |
| Rakefile             | Aquest fitxer localitza i carrega tasques que es poden executar des de la línia de comandes. Les funcions de les tasques es defineixen mitjançant els components de Rails. En lloc de modificar el `Rakefile`, hauríeu de crear les vostres pròpies tasques afegint fitxers al directori `lib/tasks` de la vostra aplicació. |
| README.md            | Aquest és un breu manual d'instruccions per a la vostra aplicació. Hauríeu d'editar aquest fitxer per explicar als altres què fa la vostra aplicació, com configurar-la, etc. |
| storage/             | Fitxers d'_Active Storage_ per al _Disk Service_. Això es tracta a [_Active Storage Overview_](https://guides.rubyonrails.org/v7.0/active_storage_overview.html).
| test/                | Proves unitàries, accessoris i altres elements de testeig. Aquests es tracten a [_Testing Rails Application_](https://guides.rubyonrails.org/v7.0/testing.html). |
| tmp/                 | Fitxers temporals (com ara la memòria cau i els fitxers pid).
| vendor/              | Un directori per a tot el codi de tercers. En una aplicació típica de Rails, això inclou les gemmes comercialitzades. |
| .gitignore           | Aquest fitxer indica a git quins directoris, fitxers i/o patrons hauria d'ignorar. Vegeu [_GitHub - Ignoring files_](https://help.github.com/articles/ignoring-files) per obtenir més informació sobre com ignorar fitxers.
| .ruby-version        | Aquest fitxer conté la versió per defecte de Ruby. |

<br/>



## 4. Hola, Rails!

Per començar, ràpidament mostrarem per la pantalla una mica de text. Per fer-ho, heu d'iniciar el vostre servidor d'aplicacions web de Rails.

<br/>



### 4.1. Iniciar el servidor web

De fet, ja teniu una aplicació Rails funcional. Per veure-ho, heu d'iniciar un servidor web a la vostra màquina de desenvolupament. Podeu fer-ho executant la següent comanda des del directori `blog`:

```bash
$ bin/rails server
```

> :warning: &nbsp; Si utilitzeu Windows, heu de passar els scripts de la carpeta `bin` directament a l'intèrpret de Ruby, per exemple `ruby bin\rails server`.

> :warning: &nbsp; La compressió d'_assets_ de JavaScript requereix que tingueu un intèrpret de JavaScript disponible al vostre sistema. En absència daquest component veureu un error `execjs` durant la compressió d'_assets_. Normalment, macOS i Windows vénen amb un intèrpret de JavaScript instal·lat. `therubyrhino` és l'intèrpret recomanat pels usuaris de JRuby i s'afegeix de manera predeterminada al `Gemfile` en les aplicacions generades amb JRuby. Podeu investigar tots els intèrprets admesos a [ExecJS](https://github.com/rails/execjs#readme).

Això iniciarà Puma, un servidor web distribuït per defecte de Rails. Per veure la vostra aplicació en acció, obriu una finestra del navegador i accediu a http://localhost:3000. Hauríeu de veure la pàgina web d'informació predeterminada de Rails:

![Captura de pantalla de la pàgina web d'inici de Rails](https://guides.rubyonrails.org/v7.0/images/getting_started/rails_welcome.png "Captura de pantalla de la pàgina web d'inici de Rails")

Quan vulgueu aturar el servidor web, premeu Ctrl+C a la finestra de la terminal on s'està executant. En l'entorn de desenvolupament, Rails generalment no requereix que reinicieu el servidor; els canvis que feu als fitxers seran recollits automàticament pel servidor.

La pàgina web d'inici de Rails és la prova de fum per a una nova aplicació de Rails: assegura que el programari està configurat correctament per publicar una pàgina web.

<br/>



### 4.2. Digues "Hola", Rails!

Perquè Rails digui "_Hola_", heu de crear com a mínim una _ruta_, un _controlador_ amb una _acció_ i una _vista_. Una ruta relaciona una petició web a una acció del controlador. Una acció del controlador realitza la feina necessària per gestionar la petició i prepara qualsevol dada per a ser mostrada. Una vista mostra les dades en el format desitjat.

Pel que fa a la implementació: les rutes són regles escrites en un Ruby [DSL (Domain-Specific Language)](https://en.wikipedia.org/wiki/Domain-specific_language). Els controladors són classes de Ruby i els seus mètodes públics són accions. I les vistes són _templates_ (plantilles), normalment escrites en una barreja d'HTML i Ruby.

Comencem afegint una ruta al nostre fitxer de rutes `config/routes.rb`, a la part superior del bloc `Rails.application.routes.draw`:

```Ruby
Rails.application.routes.draw do
  get "/articles", to: "articles#index"

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
```

La ruta anterior declara que les peticions `GET /articlesles` s'assignen a l'acció `index` del controlador `ArticlesController`.

Per crear `ArticlesController` i la seva acció `index`, executarem el generador de controladors (amb l'opció `--skip-routes` perquè ja tenim una ruta adequada):

```bash
$ bin/rails generate controller Articles index --skip-routes
```

Rails us crearà diversos fitxers:

```console
create  app/controllers/articles_controller.rb
invoke  erb
create    app/views/articles
create    app/views/articles/index.html.erb
invoke  test_unit
create    test/controllers/articles_controller_test.rb
invoke  helper
create    app/helpers/articles_helper.rb
invoke    test_unit
```

El més important d'ells és el fitxer del controlador `app/controllers/articles_controller.rb`. Fem-hi una ullada:

```ruby
class ArticlesController < ApplicationController
  def index
  end
end
```

L'acció `index` és buida. Quan una acció no renderitza explícitament una vista (o retorna una resposta HTTP d'una altra manera), Rails renderitza automàticament una vista que coincideixi amb el nom del controlador i l'acció corresponent. Convenció abans que configuració! Les vistes es troben al directori `app/views`. Així, l'acció `index` renderitzarà per defecte la vista `app/views/articles/index.html.erb`.

Obrim `app/views/articles/index.html.erb` i substituïm el seu contingut per:

```erb
<h1>Hello, Rails!</h1>
```
Si abans heu aturat el servidor web per executar el generador de controladors, reinicieu-lo amb la comanda `bin/rails server`. Ara visiteu http://localhost:3000/articles i observeu que es mostra el nostre text!

<br/>



### 4.3. Configurar la pàgina d'inici

De moment, http://localhost:3000 encara mostra "_Yay! You're on Rails!_". Farem que també es mostri el nostre text "_Hello, Rails!_" a http://localhost:3000. Per fer-ho, afegirem una ruta que _mapeji_ la ruta arrel (_root_) de la nostra aplicació amb el controlador i l'acció adequats.

Obrim `config/routes.rb`, i afegim la següent ruta `root` a la part superior del bloc `Rails.application.routes.draw`:

```ruby
Rails.application.routes.draw do
  root "articles#index"

  get "/articles", to: "articles#index"
end
```

Ara podem veure el nostre text "_Hello, Rails!_" quan visitem la direcció http://localhost:3000, confirmant que la ruta `root` també està assignada a l'acció `index` del controlador `ArticlesController`.

> :warning: &nbsp; Per obtenir més informació sobre l'_enrutament_, vegeu [_Rails Routing from the Outside In_](https://guides.rubyonrails.org/v7.0/routing.html).

<br/>



## 5. Carregar automàticament classes i mòduls

Les aplicacions de rails __no__ utilitzen `require` per carregar el codi de l'aplicació.

Potser heu notat que `ArticlesController` hereta de `ApplicationController`, però `app/controllers/articles_controller.rb` no té res semblant a:

```ruby
require "application_controller" # DON'T DO THIS.
```

Les classes i els mòduls de l'aplicació estan disponibles a tot arreu, no necessiteu i __no hauríeu__ de carregar res de l'`app` amb `require`. Aquesta funcionalitat s'anomena _càrrega automàtica_ , i podeu obtenir més informació al respecte a [_Autoloading and Reloading Constants_](https://guides.rubyonrails.org/autoloading_and_reloading_constants.html).

Només necessiteu usar la comanda `require` per a dos casos d'ús:

* Per carregar fitxers del directori `lib`.
* Per carregar les gemes que tenen `require: false` al fitxer `Gemfile`.

<br/>



## 6. MVC i tu

Fins ara, hem parlat de rutes, controladors, accions i vistes. Totes aquestes són peces típiques d'una aplicació web que segueix el [patró MVC (Model-View-Controller)](https://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93controller). MVC és un patró de disseny que divideix les responsabilitats d'una aplicació per facilitar-ne el desenvolupament. Rails segueix aquest patró de disseny per convenció.

Com que ja tenim un controlador i una vista per treballar, ara generarem la següent peça: un model.

<br/>



### 6.1. Generar un model

Un _model_ és una classe Ruby que s'utilitza per representar dades. A més, els models poden interactuar amb la base de dades de l'aplicació mitjançant una funcionalitat de Rails anomenada _Active Record_.

Per definir un model, utilitzarem el generador de models:

```bash
$ bin/rails generate model Article title:string body:text
```

> :memo: &nbsp; Els noms dels models estan en __singular__, perquè la instància d'un model representa un únic registre de dades. Per ajudar a recordar aquesta convenció, penseu en com anomenaríeu el constructor del model: volem escriure `Article.new(...)`, __no__ `Articles.new(...)`.

Això crearà diversos fitxers:

```console
invoke  active_record
create    db/migrate/<timestamp>_create_articles.rb
create    app/models/article.rb
invoke    test_unit
create      test/models/article_test.rb
create      test/fixtures/articles.yml
```

Els dos fitxers en els quals ens centrarem ara són el fitxer de migració (`db/migrate/<timestamp>_create_articles.rb`) i el fitxer del model (`app/models/article.rb`).

<br/>



### 6.2. Migracions a la base de dades

Les _migracions_ s'utilitzen per alterar l'estructura de la base de dades d'una aplicació. A les aplicacions de Rails, les migracions s'escriuen en Ruby perquè puguin ser independents de la base de dades.

Fem una ullada al contingut del nou fitxer de migració:

```ruby
class CreateArticles < ActiveRecord::Migration[7.0]
  def change
    create_table :articles do |t|
      t.string :title
      t.text :body

      t.timestamps
    end
  end
end
```

La crida a `create_table` especifica com s'ha de construir la taula `articles`. Per defecte, el mètode `create_table` afegeix una columna `id` d'increment automàtic com a clau primària. Així, el primer registre de la taula tindrà un `id` 1, el següent registre tindrà un `id` 2, i així successivament.

Dins del bloc de `create_table`, es defineixen dues columnes: `title` i `body`. Aquestes les ha afegit el generador perquè les hem inclòs a la nostra comanda (`bin/rails generate model Article title:string body:text`).

A l'última línia del bloc hi ha una definició de `t.timestamps`. Aquest mètode defineix dues columnes addicionals anomenades `created_at` i `updated_at`. Com veurem, Rails les gestionarà per nosaltres, establint els valors automàticament quan creem o actualitzem un objecte d'un model.

Executem la nostra migració amb l'ordre següent:

```bash
$ bin/rails db:migrate
```

L'ordre mostrarà un missatge indicant-nos que s'ha creat la taula correctament:

```console
==  CreateArticles: migrating ===================================
-- create_table(:articles)
   -> 0.0018s
==  CreateArticles: migrated (0.0018s) ==========================
```

> :warning: &nbsp; Per obtenir més informació sobre les migracions, consulteu [Active Record Migrations](https://guides.rubyonrails.org/v7.0/active_record_migrations.html).

Ara podem interactuar amb la taula utilitzant el nostre model.

<br/>



### 6.3. Models en la base de dades

Per jugar una mica amb el nostre model, utilitzarem una funcionalitat de Rails anomenada _consola_. La consola és una terminal interactiva amb prefix `irb`. A més a més, també carrega automàticament Rails i el nostre codi de l'aplicació.

Iniciem la consola amb aquesta comanda:

```bash
$ bin/rails console
```
Hauríeu de veure un missatge `irb` com:

```console
Loading development environment (Rails 7.0.0)
irb(main):001:0>
```

En aquesta nova terminal, podem inicialitzar un nou objecte `Article` de la següent manera:

```console
irb> article = Article.new(title: "Hello Rails", body: "I am on Rails!")
```

És important tenir en compte que només hem _inicialitzat_ aquest objecte. Aquest objecte encara no està desat a la base de dades. De moment només està disponible a la consola. Per desar l'objecte a la base de dades, hem de cridar a la funció [`save`](https://api.rubyonrails.org/v7.0.2.3/classes/ActiveRecord/Persistence.html#method-i-save):

```console
irb> article.save
(0.1ms)  begin transaction
Article Create (0.4ms)  INSERT INTO "articles" ("title", "body", "created_at", "updated_at") VALUES (?, ?, ?, ?)  [["title", "Hello Rails"], ["body", "I am on Rails!"], ["created_at", "2020-01-18 23:47:30.734416"], ["updated_at", "2020-01-18 23:47:30.734416"]]
(0.9ms)  commit transaction
=> true
```

La comanda anterior genera una consulta a la base de dades del tipus `INSERT INTO "articles" ...`. Això indica que l'article s'ha inserit a la nostra taula. Ara, si tornem a mirar l'objecte `article`, veiem que ha passat alguna cosa interessant:

```console
irb> article
=> #<Article id: 1, title: "Hello Rails", body: "I am on Rails!", created_at: "2020-01-18 23:47:30", updated_at: "2020-01-18 23:47:30">
```

Ara es mostren els atributs `id`, `created_at` i `updated_at` de l'objecte. Rails els ha omplert per nosaltres quan hem desat l'objecte.

Quan volem recuperar aquest article de la base de dades, podem cridar la funció [`find`](https://api.rubyonrails.org/v7.0.2.3/classes/ActiveRecord/FinderMethods.html#method-i-find) del model i posar l'`id` com a argument:

```console
irb> Article.find(1)
=> #<Article id: 1, title: "Hello Rails", body: "I am on Rails!", created_at: "2020-01-18 23:47:30", updated_at: "2020-01-18 23:47:30">
```

I quan volem recuperar tots els articles de la base de dades, podem cridar la funció [`all`](https://api.rubyonrails.org/v7.0.2.3/classes/ActiveRecord/Scoping/Named/ClassMethods.html#method-i-all) del model:

```console
irb> Article.all
=> #<ActiveRecord::Relation [#<Article id: 1, title: "Hello Rails", body: "I am on Rails!", created_at: "2020-01-18 23:47:30", updated_at: "2020-01-18 23:47:30">]>
```
Aquest mètode retorna un objecte [`ActiveRecord::Relation`](https://api.rubyonrails.org/v7.0.2.3/classes/ActiveRecord/Relation.html), que podeu considerar com una _array_ vitaminada.

> :warning: &nbsp; Per obtenir més informació sobre els models, vegeu [_Active Record Basics_](https://guides.rubyonrails.org/v7.0/active_record_basics.html) i [_Active Record Query Interface_](https://guides.rubyonrails.org/v7.0/active_record_querying.html).

Els models són la peça final del trencaclosques MVC. A continuació, connectarem totes les peces juntes.

<br/>



### 6.4. Llistar els articles

Tornem al nostre controlador `app/controllers/articles_controller.rb`, i canviem l'acció `index` per obtenir tots els articles de la base de dades:

```ruby
class ArticlesController < ApplicationController
  def index
    @articles = Article.all
  end
end
```

Des de la vista, podem accedir a les variables d'instància del controlador. Això vol dir que podem fer referència a `@articles` en `app/views/articles/index.html.erb`. Obrim aquest fitxer i substituïm el seu contingut per:

```erb
<h1>Articles</h1>

<ul>
  <% @articles.each do |article| %>
    <li>
      <%= article.title %>
    </li>
  <% end %>
</ul>
```

El codi anterior és una barreja d'HTML i _ERB_ . ERB és un sistema de _templates_ que avalua el codi Ruby incrustat en un document HTML. Aquí podem veure dos tipus d'etiquetes ERB: `<% %>` i `<%= %>`. L'etiqueta `<% %>` significa "avaluar el codi Ruby adjunt". L'etiqueta `<%= %>` significa "avaluar el codi Ruby adjunt i mostrar el valor que retorna". Normalment, qualsevol cosa que pugueu escriure en un programa Ruby pot anar dins d'aquestes etiquetes ERB, tot i que és millor mantenir el contingut de les etiquetes ERB el més curts possible per facilitar-ne la lectura.

Com que _no_ volem mostrar el valor retornat per `@articles.each`, hem inclòs aquest codi a `<% %>`. Però, com que _si_ que volem mostrar el valor retornat per `article.title`(per a cada article), hem inclòs aquest codi entre `<%= %>`.

Podem veure el resultat final visitant http://localhost:3000 . (Recordeu que `bin/rails server` ha d'estar en funcionament!) Això és el que passa quan fem la petició:

1. El navegador fa una petició: `GET http://localhost:3000`.
2. La nostra aplicació Rails rep aquesta sol·licitud.
3. L'_enrutador_ de Rails _mapeja_ la ruta arrel amb l'acció `indexl` del controlador `ArticlesController`.
4. L'acció `index` utilitza el model `Article` per obtenir tots els articles de la base de dades.
5. Rails representa automàticament la vista `app/views/articles/index.html.erb`.
6. El codi ERB de la vista s'avalua per generar un HTML final.
7. El servidor ens retorna una resposta al navegador que conté l'HTML final.

Hem connectat totes les peces MVC juntes i ja tenim la nostra primera acció del controlador! A continuació, passarem a la segona acció.

<br>



## 7. Operacions CRUD

Gairebé totes les aplicacions web impliquen operacions [CRUD (crear/_Create_, llegir/_Read_, actualitzar/_Update_ i eliminar/_Delete_)](https://en.wikipedia.org/wiki/Create,_read,_update,_and_delete). Fins i tot podeu observar que la majoria de les tasques que realitza la vostra aplicació són del tipus CRUD. Rails ho té present i ofereix moltes funcionalitats per ajudar a simplificar el codi en les operacions CRUD.

Comencem a explorar aquestes operacions afegint més funcionalitats a la nostra aplicació.

<br>



### 7.1. Mostrar un únic article

Actualment tenim una vista que enumera tots els articles de la nostra base de dades. Ara volem afegir una vista nova que mostri el títol i el cos d'un sol article.

Comencem afegint una nova ruta que mapeji una nova acció del controlador (que afegirem a continuació). Obriu `config/routes.rb` i inseriu l'última ruta que es mostra aquí:

```ruby
Rails.application.routes.draw do
  root "articles#index"

  get "/articles", to: "articles#index"
  get "/articles/:id", to: "articles#show"
end
```

La nova ruta és de tipus `get` una altra vegada, però té un element extra al seu _path_: l'`:id`. Això assigna un _paràmetre_ de ruta. Un paràmetre de ruta captura un segment del _path_ de la petició i posa aquest valor al Hash `params`, que és accessible des de l'acció del controlador. Per exemple, quan es gestiona la petició `GET http://localhost:3000/articles/1`, es captura `1` com el valor de l'`:id`, i llavors és accessible mitjançant `params[:id]` en l'acció `show` del controlador `ArticlesController`.

Ara afegim aquesta acció `show` a sota de l'acció `index` a `app/controllers/articles_controller.rb`:

```ruby
class ArticlesController < ApplicationController
  def index
    @articles = Article.all
  end

  def show
    @article = Article.find(params[:id])
  end
end
```

L'acció `show` crida a `Article.find`([esmentada anteriorment](https://guides.rubyonrails.org/v7.0/getting_started.html#using-a-model-to-interact-with-the-database)) amb l'ID capturat pel paràmetre de ruta. L'article retornat s'emmagatzema a la variable d'instància `@article`, de manera que és accessible des de la vista. Per defecte, l'acció `show` renderitza `app/views/articles/show.html.erb`.

Creem `app/views/articles/show.html.erb`, amb el següent contingut:

```erb
<h1><%= @article.title %></h1>

<p><%= @article.body %></p>
```

Ara podem veure l'article en concret quan visitem http://localhost:3000/articles/1 !

Per acabar, afegim una manera còmoda d'arribar a la pàgina d'un article. Enllacem el títol de cada article `app/views/articles/index.html.erb` a la seva pròpia pàgina:

```erb
<h1>Articles</h1>

<ul>
  <% @articles.each do |article| %>
    <li>
      <a href="/articles/<%= article.id %>">
        <%= article.title %>
      </a>
    </li>
  <% end %>
</ul>
```

<br>



### 7.2. Rutes mitjançant recursos

Fins ara, hem cobert la "R" (llegir/_Read_) del CRUD. Finalment cobrirem la "C" (Crear/_Create_), la "U" (Actualitzar/_Update_) i la "D" (Eliminar/_Delete_). Com haureu endevinat, ho farem afegint noves rutes, accions del controlador i vistes. Sempre que tenim aquesta combinació de rutes, accions de controlador i vistes que funcionen conjuntament per realitzar operacions CRUD en una entitat, anomenem aquesta entitat un _recurs_. Per exemple, a la nostra aplicació, diríem que un article és un recurs.

Rails proporciona un mètode de rutes anomenat [`resources`](https://api.rubyonrails.org/v7.0.2.3/classes/ActionDispatch/Routing/Mapper/Resources.html#method-i-resources) que mapeja totes les rutes convencionals per a una col·lecció de recursos, com ara articles. Per tant, abans de passar a les seccions "C", "U" i "D", substituïm les dues rutes `get` en `config/routes.rb` per `resources`:

```ruby
Rails.application.routes.draw do
  root "articles#index"

  resources :articles
end
```

Podem inspeccionar quines rutes estan _mapejades_ executant l'ordre `bin/rails routes`:

```bash
$ bin/rails routes
      Prefix Verb   URI Pattern                  Controller#Action
        root GET    /                            articles#index
    articles GET    /articles(.:format)          articles#index
 new_article GET    /articles/new(.:format)      articles#new
     article GET    /articles/:id(.:format)      articles#show
             POST   /articles(.:format)          articles#create
edit_article GET    /articles/:id/edit(.:format) articles#edit
             PATCH  /articles/:id(.:format)      articles#update
             DELETE /articles/:id(.:format)      articles#destroy
```

El mètode `resources` també genera unes funcionalitats _helper_ per a les URL i els _path_, que podem utilitzar per evitar la configuració d'una nova ruta específica. Els valors de la columna "_Prefix_" de la taula superior més un sufix `_url` o `_path` formen els noms d'aquests _helpers_. Per exemple, el _helper_ `article_path` retorna `"/articles/#{article.id}"` quan se li adjunta un article. Ara provem a utilitzar-lo per ordenar els nostres enllaços a `app/views/articles/index.html.erb`:

```erb
<h1>Articles</h1>

<ul>
  <% @articles.each do |article| %>
    <li>
      <a href="<%= article_path(article) %>">
        <%= article.title %>
      </a>
    </li>
  <% end %>
</ul>
```

No obstant això, farem un pas més enllà utilitzant el _helper_ [`link_to`](https://api.rubyonrails.org/v7.0.2.3/classes/ActionView/Helpers/UrlHelper.html#method-i-link_to). El _helper_ `link_to` renderitza un enllaç, amb el seu primer argument com a text de l'enllaç i el seu segon argument com a destinació de l'enllaç. Si passem un objecte d'un model concret com a segon argument quan cridem al _helper_ `link_to`, aquest converteix l'objecte en una ruta adequada. Per exemple, si passem un article a `link_to`, aquest cridarà la ruta `article_path`. D'aquesta manera, `app/views/articles/index.html.erb` esdevé:

```erb
<h1>Articles</h1>

<ul>
  <% @articles.each do |article| %>
    <li>
      <%= link_to article.title, article %>
    </li>
  <% end %>
</ul>
```

Perfecte!

> :warning: &nbsp; Per obtenir més informació sobre l'_enrutament_, vegeu [_Rails Routing from the Outside In_](https://guides.rubyonrails.org/v7.0/routing.html).

<br>



### 7.3. Crear un article nou

Ara passem a la "C" (Crear/_Create_) del CRUD. Normalment, a les aplicacions web, la creació d'un nou recurs és un procés que consta de diversos passos. En primer lloc, l'usuari sol·licita un formulari per poder omplir la informació del recurs. A continuació, l'usuari envia el formulari. Si no hi ha errors, es crea el recurs i es mostra algun tipus de confirmació. En cas contrari, el formulari es torna a mostrar amb els missatges d'error i es repeteix el procés.

En una aplicació de Rails, aquests passos es gestionen de manera convencional en el controlador mitjançant les accions `new` i `create`. Afegim una implementació típica d'aquestes accions a `app/controllers/articles_controller.rb`, just a sota de l'acció `show`:

```ruby
class ArticlesController < ApplicationController
  def index
    @articles = Article.all
  end

  def show
    @article = Article.find(params[:id])
  end

  def new
    @article = Article.new
  end

  def create
    @article = Article.new(title: "...", body: "...")

    if @article.save
      redirect_to @article
    else
      render :new, status: :unprocessable_entity
    end
  end
end
```

L'acció `new` crea un article nou, però no el desa. Aquest article s'utilitzarà a la vista, quan es crea el formulari. Per defecte, l'acció `new` renderitza el _template_ `app/views/articles/new.html.erb`, que crearem a continuació.

L'acció `create` crea un article nou amb els seu valors pertinents per al títol i el _body_, i intenta desar-lo. Si l'article es desa correctament, l'acció redirigeix ​​el navegador a la pàgina de l'article, per exemple a `"http://localhost:3000/articles/#{@article.id}"`. En cas contrari, l'acció torna a mostrar el formulari `app/views/articles/new.html.erb` amb el codi d'error [422 Unprocessable Entity](https://developer.mozilla.org/en-US/docs/Web/HTTP/Status/422). El títol i el _body_ que indiquem aquí són valors ficticis. Després de crear el formulari, tornarem i els canviarem.

> :information_source: &nbsp; La funció [`redirect_to`](https://api.rubyonrails.org/v7.0.2.3/classes/ActionController/Redirecting.html#method-i-redirect_to) farà que el navegador faci una nova petició, mentre que la funció [`render`](https://api.rubyonrails.org/v7.0.2.3/classes/AbstractController/Rendering.html#method-i-render) renderitza la vista especificada, per a la petició actual. És important utilitzar el `redirect_to` després de modificar la base de dades o l'estat de l'aplicació. En cas contrari, si l'usuari actualitza la pàgina, el navegador farà la mateixa petició i es repetirà la modificació de la base de dades.

<erb>



#### 7.3.1. Generador de formularis (_Form Builder_)

Utilitzarem una funció de Rails anomenada _generador de formularis_ (_Form Builder_) per crear el nostre formulari. Mitjançant un generador de formularis (_Form Builder_) i escrivint una mínima quantitat de codi, podem generar un formulari totalment configurat i que segueixi les convencions de Rails.

Creem `app/views/articles/new.html.erb` amb el següent contingut:

```erb
<h1>New Article</h1>

<%= form_with model: @article do |form| %>
  <div>
    <%= form.label :title %><br>
    <%= form.text_field :title %>
  </div>

  <div>
    <%= form.label :body %><br>
    <%= form.text_area :body %>
  </div>

  <div>
    <%= form.submit %>
  </div>
<% end %>
```

El mètode [`form_with`](https://api.rubyonrails.org/v7.0.2.3/classes/ActionView/Helpers/FormHelper.html#method-i-form_with) crea una instancia d'un generador de formularis (_Form Builder_). Tot seguit, en el bloc `form_with` cridem diferents mètodes com [`label`](https://api.rubyonrails.org/v7.0.2.3/classes/ActionView/Helpers/FormBuilder.html#method-i-label) i [`text_field`](https://api.rubyonrails.org/v7.0.2.3/classes/ActionView/Helpers/FormBuilder.html#method-i-text_field) del generador de formularis (_Form Builder_) per generar els elements corresponents.

La sortida resultant de la nostra crida a `form_with` serà semblant a aquesta:

```erb
<form action="/articles" accept-charset="UTF-8" method="post">
  <input type="hidden" name="authenticity_token" value="...">

  <div>
    <label for="article_title">Title</label><br>
    <input type="text" name="article[title]" id="article_title">
  </div>

  <div>
    <label for="article_body">Body</label><br>
    <textarea name="article[body]" id="article_body"></textarea>
  </div>

  <div>
    <input type="submit" name="commit" value="Create Article" data-disable-with="Create Article">
  </div>
</form>
```

> :warning: &nbsp; Per obtenir més informació sobre els generador de formularis (_Form Builder_), vegeu [Action View Form Helpers](https://guides.rubyonrails.org/v7.0/form_helpers.html).

<br>



#### 7.3.2. Paràmetres restringits (_Strong Parameters_)

Les dades del formulari enviades s'afegeixen al Hash `params`, juntament amb els paràmetres capturats de la ruta. Així, l'acció `create` pot accedir al títol del formulari mitjançant `params[:article][:title]` i al _body_ del formulari a través de `params[:article][:body]`. Podríem passar aquests valors individualment a `Article.new`, però això seria farragós i possiblement propens a errors. I empitjoraria a mesura que afegim més camps.

En canvi, passarem un únic Hash que contingui tots els valors. Tanmateix, encara hem d'especificar quins valors es permeten en aquest Hash. En cas de no controlar-ho, un usuari maliciós podria enviar camps addicionals dels que hi ha al formulari i sobreescriure les dades privades. De fet, si passem el Hash `params[:article]` no filtrat directament a `Article.new`, Rails generarà un error del tipus `ForbiddenAttributesError` per avisar-nos del problema. Per tant, utilitzarem una característica de Rails anomenada _Strong Parameters_ (_paràmetres restringits_) per filtrar `params`. Penseu en això com si utilitzéssiu un [llenguatge fortament tipat](https://en.wikipedia.org/wiki/Strong_and_weak_typing) per a `params`.

Afegim un mètode privat a la part inferior del controlador `app/controllers/articles_controller.rb` anomenat `article_params` que filtrarà els `params`. I canviem `create` per tal d'utilitzar-lo:

```ruby
class ArticlesController < ApplicationController
  def index
    @articles = Article.all
  end

  def show
    @article = Article.find(params[:id])
  end

  def new
    @article = Article.new
  end

  def create
    @article = Article.new(article_params)

    if @article.save
      redirect_to @article
    else
      render :new, status: :unprocessable_entity
    end
  end

  private
    def article_params
      params.require(:article).permit(:title, :body)
    end
end
```

> :warning: Per obtenir més informació sobre els _paràmetres restringits_, vegeu [_Action Controller Overview § Strong Parameters_](https://guides.rubyonrails.org/v7.0/action_controller_overview.html#strong-parameters).

<br>



#### 7.3.3. Validacions i missatges d'error

Com hem vist, crear un recurs és un procés de diversos passos. La gestió de les entrades d'usuari no vàlides és un altre pas d'aquest procés. Rails ofereix una funció anomenada _validacions_ per ajudar-nos a gestionar les entrades d'usuari no vàlides. Les validacions són regles que es comproven abans de desar un objecte d'un model. Si alguna de les comprovacions falla, l'acció de desar s'avortarà i s'afegiran els missatges d'error adequats a l'atribut `errors` de l'objecte.

Afegim algunes validacions al nostre model al fitxer `app/models/article.rb`:

```ruby
class Article < ApplicationRecord
  validates :title, presence: true
  validates :body, presence: true, length: { minimum: 10 }
end
```

La primera validació declara que `title` ha d'estar present. Com que `title` és una cadena, això significa que el valor `title` ha de contenir almenys un caràcter que no sigui un espai en blanc.

La segona validació declara que `body` també ha d'estar present. A més, declara que el valor `body` ha de tenir almenys 10 caràcters.

> :information_source: &nbsp; Potser us preguntareu on es defineixen els atributs `title` i `body`. _Active Record_ defineix automàticament els atributs del model per a cada columna de la taula, de manera que no cal que declarar aquests atributs al fitxer del model.

Amb les nostres validacions en seu lloc corresponent, modifiquem `app/views/articles/new.html.erb` per mostrar els missatges d'error de `title` i de `body`:

```erb
<h1>New Article</h1>

<%= form_with model: @article do |form| %>
  <div>
    <%= form.label :title %><br>
    <%= form.text_field :title %>
    <% @article.errors.full_messages_for(:title).each do |message| %>
      <div><%= message %></div>
    <% end %>
  </div>

  <div>
    <%= form.label :body %><br>
    <%= form.text_area :body %><br>
    <% @article.errors.full_messages_for(:body).each do |message| %>
      <div><%= message %></div>
    <% end %>
  </div>

  <div>
    <%= form.submit %>
  </div>
<% end %>
```

El mètode [`full_messages_for`](https://api.rubyonrails.org/v7.0.2.3/classes/ActiveModel/Errors.html#method-i-full_messages_for) retorna un _array_ de missatges d'error fàcils d'utilitzar per a l'atribut específic. Si no hi ha errors per aquest atribut, la matriu estarà buida.

Per entendre com funciona tot plegat, fem una altra ullada a les accions `new` i `create` del controlador:

```ruby
  def new
    @article = Article.new
  end

  def create
    @article = Article.new(article_params)

    if @article.save
      redirect_to @article
    else
      render :new, status: :unprocessable_entity
    end
  end
```

Quan visitem http://localhost:3000/articles/new, la petició `GET /articles/new` s'assigna a l'acció `new`. L'acció `new` no intenta desar l'`@article`. Per tant, les validacions no es comproven i no hi haurà missatges d'error.

Quan enviem el formulari, la petició `POST /articles` s'assigna a l'acció `create`. L'acció `create` intenta salvar. Per tant, es comproven les validacions d'`@article`. Si alguna validació falla, no es desarà i es mostrarà amb missatges d'error el _template_ `@articleapp/views/articles/new.html.erb`.

> :warning: &nbsp; Per obtenir més informació sobre les validacions, vegeu [_Active Record Validations_](https://guides.rubyonrails.org/v7.0/active_record_validations.html). Per obtenir més informació sobre els missatges d'error de validació, vegeu [_ Active Record Validations § Working with Validation Errors_](https://guides.rubyonrails.org/v7.0/active_record_validations.html#working-with-validation-errors).

<br>



#### 7.3.4. Últims detalls

Ara podem crear un article visitant la ruta http://localhost:3000/articles/new. Per acabar, enllacem a aquesta pàgina des de la part inferior del _template_ `app/views/articles/index.html.erb`:

```erb
<h1>Articles</h1>

<ul>
  <% @articles.each do |article| %>
    <li>
      <%= link_to article.title, article %>
    </li>
  <% end %>
</ul>

<%= link_to "New Article", new_article_path %>
```

<br>



### 7.4. Actualitzar un article

Hem cobert el "CR" del CRUD. Ara passem a la "U" (Actualització/_Update_). Actualitzar un recurs és molt semblant a crear un recurs. Tots dos són processos que consten de diversos passos. En primer lloc, l'usuari sol·licita un formulari per editar les dades. A continuació, l'usuari envia el formulari. Si no hi ha errors, el recurs s'actualitza. En cas contrari, el formulari es torna a mostrar amb missatges d'error i el procés es repeteix.

Aquests passos són gestionats convencionalment en el controlador per les accions `edit` i `update`. Afegim una implementació típica d'aquestes accions a `app/controllers/articles_controller.rb`, just a sota de l'acció `create`:

```ruby
class ArticlesController < ApplicationController
  def index
    @articles = Article.all
  end

  def show
    @article = Article.find(params[:id])
  end

  def new
    @article = Article.new
  end

  def create
    @article = Article.new(article_params)

    if @article.save
      redirect_to @article
    else
      render :new, status: :unprocessable_entity
    end
  end

  def edit
    @article = Article.find(params[:id])
  end

  def update
    @article = Article.find(params[:id])

    if @article.update(article_params)
      redirect_to @article
    else
      render :edit, status: :unprocessable_entity
    end
  end

  private
    def article_params
      params.require(:article).permit(:title, :body)
    end
end
```

Observeu com les accions `edit` i `update` s'assemblen a les accions `new` i `create`.

L'acció `edit` recupera l'article de la base de dades i l'emmagatzema a la variable d'instància `@article` perquè es pugui utilitzar quan es construeixi el formulari. Per defecte, l'acció `edit` renderitza el _template_ `app/views/articles/edit.html.erb`.

L'acció `update` (torna a) recupera l'article de la base de dades i intenta actualitzar-lo amb les dades del formulari enviat filtrades per `article_params`. Si cap validació falla i l'actualització té èxit, l'acció redirigeix ​​el navegador a la pàgina de l'article. En cas contrari, l'acció torna a mostrar el formulari (amb els corresponents missatges d'error) mostrant el _template_ `app/views/articles/edit.html.erb`.

<br>



#### 7.4.1. Parcials en les vistes

El nostre formulari `edit` tindrà el mateix aspecte que el nostre formulari `new`. Fins i tot el codi serà el mateix, gràcies al generador de formularis (_Form Builder_) de Rails i a l'enrutament mitjançant recursos. El generador de formularis (_Form Builder_) configura automàticament el formulari per crear el tipus de petició adequada, en funció de si l'objecte s'ha desat prèviament.

Com que el codi serà el mateix, el _refactoritzarem_ en una vista compartida anomenada _parcial_. Creem `app/views/articles/_form.html.erb` amb el següent contingut:

```erb
<%= form_with model: article do |form| %>
  <div>
    <%= form.label :title %><br>
    <%= form.text_field :title %>
    <% article.errors.full_messages_for(:title).each do |message| %>
      <div><%= message %></div>
    <% end %>
  </div>

  <div>
    <%= form.label :body %><br>
    <%= form.text_area :body %><br>
    <% article.errors.full_messages_for(:body).each do |message| %>
      <div><%= message %></div>
    <% end %>
  </div>

  <div>
    <%= form.submit %>
  </div>
<% end %>
```

El codi anterior és el mateix que tenim al nostre formulari `app/views/articles/new.html.erb`, excepte que totes les ocurrències d'`@article` s'han substituït per `article`. Com que els parcials són codi compartit, la millor pràctica és que no depenguin de variables d'instància específiques establertes per una acció del controlador. En canvi, passarem l'article al parcial com a variable local.

Actualitzem `app/views/articles/new.html.erb` perquè utilitzi el parcial, mitjançant el mètode [`render`](https://api.rubyonrails.org/v7.0.2.3/classes/ActionView/Helpers/RenderingHelper.html#method-i-render):

```erb
<h1>New Article</h1>

<%= render "form", article: @article %>
```

> :memo: &nbsp; El nom del fitxer d'un parcial ha d'anar prefixat __amb__ un guió baix, per exemple `_form.html.erb`. Però quan es crida, es fa referència __sense__ el guió baix, per exemple `render "form"`.

I ara, anem a fer gairebé el mateix a `app/views/articles/edit.html.erb`:

```erb
<h1>Edit Article</h1>

<%= render "form", article: @article %>
```

> :warning: &nbsp; Per obtenir més informació sobre els parcials, vegeu [_ Layouts and Rendering in Rails § Using Partials_](https://guides.rubyonrails.org/v7.0/layouts_and_rendering.html#using-partials).

<br>



#### 7.4.2. Últims detalls

Ara podem modificar un article visitant la seva pàgina d'edició, ex: http://localhost:3000/articles/1/edit . Per acabar, enllacem a la pàgina d'edició des de la part inferior del _template_ `app/views/articles/show.html.erb`:

```erb
<h1><%= @article.title %></h1>

<p><%= @article.body %></p>

<ul>
  <li><%= link_to "Edit", edit_article_path(@article) %></li>
</ul>
```

<br>



### 7.5. Eliminar un article

Finalment arribem a la "D" (Eliminar) del CRUD. Suprimir un recurs és un procés més senzill que crear o actualitzar. Només requereix una ruta i una acció en el controlador. I el nostre enrutament del recurs article (`resources :articles`) ja proporciona la ruta que mapeja `DELETE /articles/:id` i les peticions a l'acció `destroy` del controlador `ArticlesController`.

Per tant, afegim una acció `destroy` típica a `app/controllers/articles_controller.rb`, a sota de l'acció `update`:

```ruby
class ArticlesController < ApplicationController
  def index
    @articles = Article.all
  end

  def show
    @article = Article.find(params[:id])
  end

  def new
    @article = Article.new
  end

  def create
    @article = Article.new(article_params)

    if @article.save
      redirect_to @article
    else
      render :new, status: :unprocessable_entity
    end
  end

  def edit
    @article = Article.find(params[:id])
  end

  def update
    @article = Article.find(params[:id])

    if @article.update(article_params)
      redirect_to @article
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    @article = Article.find(params[:id])
    @article.destroy

    redirect_to root_path, status: :see_other
  end

  private
    def article_params
      params.require(:article).permit(:title, :body)
    end
end
```

L'acció `destroy` busca l'article a la base de dades i li aplica el mètode [`destroy`](https://api.rubyonrails.org/v7.0.2.3/classes/ActiveRecord/Persistence.html#method-i-destroy). A continuació, redirigeix ​​el navegador a la ruta arrel amb el codi d'estat [_303 See Other_](https://developer.mozilla.org/en-US/docs/Web/HTTP/Status/303).

Hem escollit redirigir a la ruta arrel perquè aquest és el nostre principal punt d'accés per als articles. Però, en altres circumstàncies, podeu optar per redirigir per exemple a `articles_path`.

Ara afegim un enllaç a la part inferior del _template_ `app/views/articles/show.html.erb` perquè puguem eliminar un article des de la seva pròpia pàgina:

```erb
<h1><%= @article.title %></h1>

<p><%= @article.body %></p>

<ul>
  <li><%= link_to "Edit", edit_article_path(@article) %></li>
  <li><%= link_to "Destroy", article_path(@article), data: {
                    turbo_method: :delete,
                    turbo_confirm: "Are you sure?"
                  } %></li>
</ul>
```

Al codi anterior, hem utilitzat l'opció `data` per establir els atributs HTML `data-turbo-method` i `data-turbo-confirm` de l'enllaç "Destroy". Tots dos atributs pertanyen a [Turbo](https://turbo.hotwired.dev/), que s'inclou per defecte a les noves aplicacions de Rails. `data-turbo-method="delete"` farà que l'enllaç faci una sol·licitud `DELETE` en lloc d'una sol·licitud `GET`. `data-turbo-confirm="Are you sure?"` farà que aparegui un diàleg de confirmació quan es faci clic a l'enllaç. Si l'usuari cancel·la el diàleg, la sol·licitud s'avortarà.

I ja està! Ara podem llistar, mostrar, crear, actualitzar i suprimir articles! Increïble!

<br>



## 8. Afegir un segon model

És hora d'afegir un segon model a l'aplicació. El segon model gestionarà els comentaris dels articles.

<br>



### 8.1. Generar un nou model

Veurem el mateix generador que hem utilitzat abans quan hem creat el model `Article`. Aquesta vegada crearem un model `Comment` que contindrà una referència a un article. Executeu aquesta comanda a la vostra terminal:

```bash
$ bin/rails generate model Comment commenter:string body:text article:references
```

Aquesta comanda generarà quatre fitxers:

| Dossier	                                     | Propòsit                                                                                                                       |
| ---                                          | ---                                                                                                                            |
| db/migrate/20140120201010_create_comments.rb | Migració per crear la taula de comentaris a la vostra base de dades (el vostre nom de fitxer inclourà un _timestamp_ diferent) |
| app/models/comment.rb	                       | El model de comentaris                                                                                                         |
| test/models/comment_test.rb	                 | Fitxer dels tests per al model de comentaris                                                                                   |
| prova/fixtures/comments.yml	                 | Exemples de comentaris per utilitzar-los en els tests                                                                          |

Primer, fes una ullada a `app/models/comment.rb`:

```ruby
class Comment < ApplicationRecord
  belongs_to :article
end
```

És molt semblant al model `Article` que heu vist abans. La diferència es troba a la línia `belongs_to :article`, que configura una _relació Active Record_. Aprendràs una mica sobre les relacions a la següent secció d'aquesta guia.

La paraula clau (`:references`) utilitzada en la comanda anterior és un tipus de dades especial per als models. Crea una columna nova a la taula de la vostra base de dades amb el nom del model proporcionat acabat amb el sufix `_id` que pot contenir valors enters. Per entendre-ho millor, analitzeu el fitxer `db/schema.rb` després d'executar la migració.

A més del model, Rails també ha creat un fitxer de migració per a persistir la taula corresponent a la base de dades:

```ruby
class CreateComments < ActiveRecord::Migration[7.0]
  def change
    create_table :comments do |t|
      t.string :commenter
      t.text :body
      t.references :article, null: false, foreign_key: true

      t.timestamps
    end
  end
end
```

La línia `t.references` crea una columna entera (_integer_) anomenada `article_id`, un índex per a aquesta i una _foreign key_ que apunta a la columna `id` de la taula `articles`. Avança i executa la migració:

```bash
$ bin/rails db:migrate
```

Rails és prou intel·ligent com per executar només les migracions que encara no s'han executat a la base de dades actual, així que en aquest cas només veureu:

```console
==  CreateComments: migrating =================================================
-- create_table(:comments)
   -> 0.0115s
==  CreateComments: migrated (0.0119s) ========================================
```

<br>



### 8.2. Relacionar models

Les relacions _Active Record_ us permeten declarar fàcilment la relació entre dos models. En el cas de comentaris i articles, podeu definir les relacions d'aquesta manera:

* Cada comentari pertany a un article.
* Un article pot tenir molts comentaris.

De fet, això és molt semblant a la sintaxi que utilitza Rails per declarar aquesta relació. La línia de codi dins del model `Comment` (`app/models/comment.rb`) que fa que cada comentari pertanyi a un article és `belongs_to :article`:

```ruby
class Comment < ApplicationRecord
  belongs_to :article
end
```

També haureu d'editar `app/models/article.rb` per afegir l'altra cara de la relació:

```ruby
class Article < ApplicationRecord
  has_many :comments

  validates :title, presence: true
  validates :body, presence: true, length: { minimum: 10 }
end
```

Aquestes dues declaracions permeten un bon comportament automàtic de Rails i establir correctament la relació dels dos elements. Per exemple, si teniu una variable d'instància `@article` que conté un article, podeu recuperar tots els comentaris que pertanyen a aquest article com un _array_ mitjançant l'expressió `@article.comments`.

> :warning: &nbsp; Per obtenir més informació sobre les relacions _Active Record_, consulteu la guia [_Active Record Associations_](https://guides.rubyonrails.org/v7.0/association_basics.html).

<br>



### 8.3. Afegir una ruta relacionada

Igual que amb el controlador `articles`, haurem d'afegir una ruta perquè Rails sàpiga on ens agradaria navegar per veure els comentaris (`comments`). Torneu a obrir el fitxer `config/routes.rb` i editeu-lo de la següent manera:

```ruby
Rails.application.routes.draw do
  root "articles#index"

  resources :articles do
    resources :comments
  end
end
```

Això ens indica que `comments` és un _recurs relacionat_ a `articles`. Aquest és un pas més en el procés de generar la relació jeràrquica que hi ha entre articles i comentaris.

> :warning: &nbsp; Per obtenir més informació sobre el procés de generar les rutes, consulteu la guia [_Rails Routing_](https://guides.rubyonrails.org/v7.0/routing.html).

<br>



### 8.4. Generar un nou controlador

Amb el model ja creat, podeu centrar la vostra atenció en crear el controlador corresponent. De nou, farem servir el mateix generador que hem utilitzat abans:

```bash
$ bin/rails generate controller Comments
```

Això crea quatre fitxers i un directori buit:

| Fitxer/Directori                             | Propòsit                                       |
| ---                                          | ---                                            |
| app/controllers/comments_controller.rb       | El controlador dels comentaris                 |
| app/views/comments/                          | Les vistes del controlador es guarden aquí     |
| test/controllers/comments_controller_test.rb | Els tests del controlador                      |
| app/helpers/comments_helper.rb               | Un fitxer _helper_ per a la vista corresponent |
| app/assets/stylesheets/comments.scss         | Full d'estil en cascada per al controlador     |

Com a qualsevol Blog, els nostres lectors voldran escriure els seus comentaris directament després de llegir l'article i, una vegada que l'hagin afegit i guardat, seran enviats de nou a la pàgina de l'article corresponent on ara si que el podran veure publicat. Per això hem de crear el controlador `CommentsController` i és allà on hi ha d'haver un mètode per crear comentaris i eliminar comentaris de _spam_ quan arribin.

Per tant, primer de tot modifiquem el _template_ de l'article (`app/views/articles/show.html.erb`) per permetre'ns fer un comentari nou:

```erb
<h1><%= @article.title %></h1>

<p><%= @article.body %></p>

<ul>
  <li><%= link_to "Edit", edit_article_path(@article) %></li>
  <li><%= link_to "Destroy", article_path(@article), data: {
                    turbo_method: :delete,
                    turbo_confirm: "Are you sure?"
                  } %></li>
</ul>

<h2>Add a comment:</h2>
<%= form_with model: [ @article, @article.comments.build ] do |form| %>
  <p>
    <%= form.label :commenter %><br>
    <%= form.text_field :commenter %>
  </p>
  <p>
    <%= form.label :body %><br>
    <%= form.text_area :body %>
  </p>
  <p>
    <%= form.submit %>
  </p>
<% end %>
```

Com que hem creat un nou formulari a la pàgina `show` de l'`Article`, hem de crear un comentari nou cridant l'acció `create` del `CommentsController`. El _helper_ `form_with` utilitza un _array_ de comentaris i generarà una ruta enllaçada com ara `/articles/1/comments`.

Creem l'acció `create` a `app/controllers/comments_controller.rb`:

```ruby
class CommentsController < ApplicationController
  def create
    @article = Article.find(params[:article_id])
    @comment = @article.comments.create(comment_params)
    redirect_to article_path(@article)
  end

  private
    def comment_params
      params.require(:comment).permit(:commenter, :body)
    end
end
```

Aquí veiem una mica més de complexitat que al controlador d'articles. Aquest és un efecte secundari de la relació que heu configurat. Per cada sol·licitud d'un comentari s'ha de fer una cerca de l'article al qual s'adjunta, per tant també es crida al mètode `find` del model `Article` per obtenir l'article en qüestió.

A més, el codi aprofita alguns dels mètodes disponibles en el cas d'una relació. Utilitzem el mètode `create` a `@article.comments` per crear i desar el comentari. Això enllaçarà automàticament el comentari amb l'article perquè pertanyi a aquest article en concret.

Un cop hem creat el nou comentari, reenviem a l'usuari a l'article original mitjançant el _helper_ `article_path(@article)`. Com ja hem vist, això crida l'acció `show` del `ArticlesController` que al seu torn mostra el _template_ `show.html.erb`. Aquí és on volem que es mostri el comentari, així que afegim-ho a `app/views/articles/show.html.erb`.

```erb
<h1><%= @article.title %></h1>

<p><%= @article.body %></p>

<ul>
  <li><%= link_to "Edit", edit_article_path(@article) %></li>
  <li><%= link_to "Destroy", article_path(@article), data: {
                    turbo_method: :delete,
                    turbo_confirm: "Are you sure?"
                  } %></li>
</ul>

<h2>Comments</h2>
<% @article.comments.each do |comment| %>
  <p>
    <strong>Commenter:</strong>
    <%= comment.commenter %>
  </p>

  <p>
    <strong>Comment:</strong>
    <%= comment.body %>
  </p>
<% end %>

<h2>Add a comment:</h2>
<%= form_with model: [ @article, @article.comments.build ] do |form| %>
  <p>
    <%= form.label :commenter %><br>
    <%= form.text_field :commenter %>
  </p>
  <p>
    <%= form.label :body %><br>
    <%= form.text_area :body %>
  </p>
  <p>
    <%= form.submit %>
  </p>
<% end %>
```

Ara pots afegir articles i comentaris al teu Blog i fer que es mostrin adequadament.

![Article amb comentaris](https://guides.rubyonrails.org/v7.0/images/getting_started/article_with_comments.png "Article amb comentaris")

<br>



## 9. Refactoritzar

Ara que tenim ja articles i comentaris funcionant, feu una ullada al _template_ `app/views/articles/show.html.erb`. S'està fent llarg i incòmode de llegir. Podem utilitzar els parcials per a netejar-lo.

<br>



### 9.1. Parcials per les col·leccions

Primer, farem un parcial per als comentaris i així poder mostrant tots els comentaris de l'article. Creeu el fitxer `app/views/comments/_comment.html.erb` i introduïu-hi el següent:

```erb
<p>
  <strong>Commenter:</strong>
  <%= comment.commenter %>
</p>

<p>
  <strong>Comment:</strong>
  <%= comment.body %>
</p>
```

Aleshores, podeu canviar `app/views/articles/show.html.erb` de la següent manera:

```erb
<h1><%= @article.title %></h1>

<p><%= @article.body %></p>

<ul>
  <li><%= link_to "Edit", edit_article_path(@article) %></li>
  <li><%= link_to "Destroy", article_path(@article), data: {
                    turbo_method: :delete,
                    turbo_confirm: "Are you sure?"
                  } %></li>
</ul>

<h2>Comments</h2>
<%= render @article.comments %>

<h2>Add a comment:</h2>
<%= form_with model: [ @article, @article.comments.build ] do |form| %>
  <p>
    <%= form.label :commenter %><br>
    <%= form.text_field :commenter %>
  </p>
  <p>
    <%= form.label :body %><br>
    <%= form.text_area :body %>
  </p>
  <p>
    <%= form.submit %>
  </p>
<% end %>
```

Això ara ens renderitza el parcial `app/views/comments/_comment.html.erb` una vegada per a cada comentari que hi ha a la col·lecció `@article.comments`. A mesura que el mètode `render` itera sobre la col·lecció `@article.comments`, assigna a cada comentari una variable local anomenada igual que el parcial, en aquest cas `comment`, que després estarà disponible en el parcial per mostrar la informació que desitgem.

<br>



### 9.2. Parcials pels formularis

Movem també aquesta nova secció de comentaris a la seu pròpi parcial. De nou, creeu un fitxer `app/views/comments/_form.html.erb` que contingui:

```erb
<%= form_with model: [ @article, @article.comments.build ] do |form| %>
  <p>
    <%= form.label :commenter %><br>
    <%= form.text_field :commenter %>
  </p>
  <p>
    <%= form.label :body %><br>
    <%= form.text_area :body %>
  </p>
  <p>
    <%= form.submit %>
  </p>
<% end %>
```

A continuació, modifiqueu el fitxer `app/views/articles/show.html.erb` de la següent manera:

```erb
<h1><%= @article.title %></h1>

<p><%= @article.body %></p>

<ul>
  <li><%= link_to "Edit", edit_article_path(@article) %></li>
  <li><%= link_to "Destroy", article_path(@article), data: {
                    turbo_method: :delete,
                    turbo_confirm: "Are you sure?"
                  } %></li>
</ul>

<h2>Comments</h2>
<%= render @article.comments %>

<h2>Add a comment:</h2>
<%= render 'comments/form' %>
```

El segon render només crida el _template_ del parcial que volem renderitzar, `comments/form`. Rails és prou intel·ligent per detectar la barra inclinada (`/`) i adonar-se que voleu representar el fitxer `_form.html.erb` del directori `app/views/comments`.

L'objecte `@article` està disponible per a qualsevol parcial que es renderitzi en aquesta vista, perquè l'hem definit com una variable d'instància.

<br>



### 9.3. Els _Concerns_

Els _concerns_ són una funcionalitat que ens permet que els controladors o models grans siguin més fàcils d'entendre i gestionar. També ens proporcionen la _reutilització del codi_ quan diversos models (o controladors) comparteixen els mateixos _concerns_. Els _concerns_ s'implementen mitjançant mòduls que contenen mètodes que defineixen una porció ben definida de la funcionalitat de la qual és responsable un model o controlador. En altres llenguatges, els mòduls es coneixen sovint com a _mixins_.

Podeu utilitzar els _concerns_ al controlador o al model de la mateixa manera que utilitzaríeu qualsevol mòdul. Quan vau crear la vostra aplicació per primera vegada amb la comanda `rails new blog`, es van crear dues carpetes a `app/` juntament amb la resta:

```console
app/controllers/concerns
app/models/concerns
```

Un article determinat del Blog pot tenir diferents estats; per exemple, pot ser visible per a tothom (ex. `public`), o només visible per a l'autor (ex. `private`). També pot estar amagat per a tothom, però es pot recuperar (és a dir `archived`). Els comentaris també poden estar ocults o visibles. Això es podria representar mitjançant una columna `status` a cada model.

Primer creem les migracions següents per afegir la columna `status` a `Articles` i `Comments`:

```bash
$ bin/rails generate migration AddStatusToArticles status:string
$ bin/rails generate migration AddStatusToComments status:string
```

I a continuació, actualitzem la base de dades aplicant les migracions corresponents:

```bash
$ bin/rails db:migrate
```

> :warning: &nbsp; Per obtenir més informació sobre les migracions, consulteu [_Active Record Migrations_](https://guides.rubyonrails.org/v7.0/active_record_migrations.html).

També hem d'incloure la clau `:status` com a un dels _paràmetres restringits_ a `app/controllers/articles_controller.rb`:

```ruby
  private

  def article_params
    params.require(:article).permit(:title, :body, :status)
  end
```

I a `app/controllers/comments_controller.rb`:

```ruby
  private

  def comment_params
    params.require(:comment).permit(:commenter, :body, :status)
  end
```

Dins del model `article`, després d'executar una migració per afegir una columna `status` mitjançant l'ordre `bin/rails db:migrate`, afegireu:

```ruby
class Article < ApplicationRecord
  has_many :comments

  validates :title, presence: true
  validates :body, presence: true, length: { minimum: 10 }

  VALID_STATUSES = ['public', 'private', 'archived']

  validates :status, inclusion: { in: VALID_STATUSES }

  def archived?
    status == 'archived'
  end
end
```

i en el model `Comment`:

```ruby
class Comment < ApplicationRecord
  belongs_to :article

  VALID_STATUSES = ['public', 'private', 'archived']

  validates :status, inclusion: { in: VALID_STATUSES }

  def archived?
    status == 'archived'
  end
end
```

Aleshores, a la nostra plantilla `index` a `app/views/articles/index.html.erb` utilitzarem el mètode `archived?` per evitar que es mostri cap article que estigui arxivat:

```erb
<h1>Articles</h1>

<ul>
  <% @articles.each do |article| %>
    <% unless article.archived? %>
      <li>
        <%= link_to article.title, article %>
      </li>
    <% end %>
  <% end %>
</ul>

<%= link_to "New Article", new_article_path %>
```

De la mateixa manera, a la nostra vista parcial de comentaris (`app/views/comments/_comment.html.erb`) utilitzarem el mètode `archived?` per evitar que es mostri cap comentari que estigui arxivat:

```erb
<% unless comment.archived? %>
  <p>
    <strong>Commenter:</strong>
    <%= comment.commenter %>
  </p>

  <p>
    <strong>Comment:</strong>
    <%= comment.body %>
  </p>
<% end %>
```

Tanmateix, si ara torneu a mirar els nostres models, podreu veure que la lògica està duplicada. Si en el futur augmentem la funcionalitat del nostre bloc, per exemple per incloure missatges privats, ens podríem trobar duplicant la lògica una altra vegada. Aquí és on els _concerns_ són útils.

Un _concern_ només és responsable d'un subconjunt concret del model que l'implementa; d'aquesta manera, tots els mètodes del _concern_ seran visibles des del model que l'implementa. Anomenem el nostre nou _concern_ (mòdul) `Visible`. Podem crear un fitxer nou dins el directori `app/models/concerns` anomenat `visible.rb`, i crear el mètode d'estat que s'utilitzarà en els diferents models.

`app/models/concerns/visible.rb`

```ruby
module Visible
  def archived?
    status == 'archived'
  end
end
```

Podem afegir la nostra validació d'estat al _concern_, però això és una mica més complex, ja que les validacions són mètodes que es criden al nivell de la classe. El mòdul `ActiveSupport::Concern`([Guia de l'API](https://api.rubyonrails.org/v7.0.2.3/classes/ActiveSupport/Concern.html)) ens ofereix una manera més senzilla d'incloure-les:

```ruby
module Visible
  extend ActiveSupport::Concern

  VALID_STATUSES = ['public', 'private', 'archived']

  included do
    validates :status, inclusion: { in: VALID_STATUSES }
  end

  def archived?
    status == 'archived'
  end
end
```

Ara podem eliminar la lògica duplicada de cada model i incloure el nostre nou mòdul `Visible`:

A `app/models/article.rb`:

```ruby
class Article < ApplicationRecord
  include Visible

  has_many :comments

  validates :title, presence: true
  validates :body, presence: true, length: { minimum: 10 }
end
```

I a `app/models/comment.rb`:

```ruby
class Comment < ApplicationRecord
  include Visible

  belongs_to :article
end
```

Els mètodes de classe també es poden afegir als _concerns_. Si volem mostrar un comptador d'articles o comentaris públics a la nostra pàgina principal, podríem afegir un mètode de classe al mòdul `Visible` de la següent manera:

```ruby
module Visible
  extend ActiveSupport::Concern

  VALID_STATUSES = ['public', 'private', 'archived']

  included do
    validates :status, inclusion: { in: VALID_STATUSES }
  end

  class_methods do
    def public_count
      where(status: 'public').count
    end
  end

  def archived?
    status == 'archived'
  end
end
```

Aleshores, a la vista, es pot cridar com a qualsevol mètode de classe:

```erb
<h1>Articles</h1>

Our blog has <%= Article.public_count %> articles and counting!

<ul>
  <% @articles.each do |article| %>
    <% unless article.archived? %>
      <li>
        <%= link_to article.title, article %>
      </li>
    <% end %>
  <% end %>
</ul>

<%= link_to "New Article", new_article_path %>
```

Per acabar, afegirem un selector als formularis, i deixarem que l'usuari seleccioni l'estat quan creï un nou article o publiqui un nou comentari. També podem especificar que l'estat predeterminat sigui `public`. A `app/views/articles/_form.html.erb`, podem afegir:

```erb
<div>
  <%= form.label :status %><br>
  <%= form.select :status, ['public', 'private', 'archived'], selected: 'public' %>
</div>
```

i en `app/views/comments/_form.html.erb`:

```erb
<p>
  <%= form.label :status %><br>
  <%= form.select :status, ['public', 'private', 'archived'], selected: 'public' %>
</p>
```

<br>



## 10. Eliminar comentaris

Una altra característica important d'un bloc és poder eliminar els comentaris de _spam_. Per fer-ho, hem d'implementar un enllaç d'algun tipus a la vista i una acció `destroy` al fitxer `CommentsController`.

Per tant, primer, afegim l'enllaç d'eliminació de comentaris al parcial `app/views/comments/_comment.html.erb`:

```erb
<p>
  <strong>Commenter:</strong>
  <%= comment.commenter %>
</p>

<p>
  <strong>Comment:</strong>
  <%= comment.body %>
</p>

<p>
  <%= link_to "Destroy Comment", [comment.article, comment], data: {
                turbo_method: :delete,
                turbo_confirm: "Are you sure?"
              } %>
</p>
```

Si feu clic a aquest nou enllaç "Destroy Comment", es llençarà una petició `DELETE
/articles/:article_id/comments/:id` al nostre controlador `CommentsController`, que la utilitzarà per trobar el comentari que volem suprimir. Així que haurem d'afegir una acció `destroy` al nostre controlador (`app/controllers/comments_controller.rb`):

```ruby
class CommentsController < ApplicationController
  def create
    @article = Article.find(params[:article_id])
    @comment = @article.comments.create(comment_params)
    redirect_to article_path(@article)
  end

  def destroy
    @article = Article.find(params[:article_id])
    @comment = @article.comments.find(params[:id])
    @comment.destroy
    redirect_to article_path(@article), status: 303
  end

  private
    def comment_params
      params.require(:comment).permit(:commenter, :body, :status)
    end
end
```

L'acció `destroy` trobarà l'article que estem mirant, localitzarà el comentari dins de la col·lecció `@article.comments` i, a continuació, l'eliminarà de la base de dades i ens enviarà de nou a l'acció `show` de l'article.

<br>



### 10.1. Eliminar objectes relacionats

Si suprimiu un article, també s'haurien d'eliminar els seus comentaris relacionats, en cas contrari simplement ocuparien espai a la base de dades. Rails us permet utilitzar l'opció `dependent` d'una relació per aconseguir-ho. Modifiqueu el model d'article, `app/models/article.rb`, de la següent manera:

```ruby
class Article < ApplicationRecord
  include Visible

  has_many :comments, dependent: :destroy

  validates :title, presence: true
  validates :body, presence: true, length: { minimum: 10 }
end
```

<br>



## 11. Seguretat

<br>



### 11.1. Autenticació bàsica

Si publiqueu el vostre bloc _online_, qualsevol persona podria afegir, editar i suprimir articles o eliminar comentaris.

Rails proporciona un sistema d'autenticació HTTP que pot funcionar bé en aquests casos.

En el controlador `ArticlesController` cal crear un mètode que ens permeti limitar l'ús de les diferents accions si la persona no està autenticada. D'aquesta manera podem utilitzar el mètode `http_basic_authenticate_with` de Rails, que permet executar l'acció sol·licitada només si aquest mètode ho permet.

Per utilitzar el sistema d'autenticació, l'especifiquem a la part superior del nostre controlador `ArticlesController` a `app/controllers/articles_controller.rb`. En el nostre cas, volem que l'usuari s'autentiqui per a cada acció excepte `index` i `show`, així que escriurem:

```ruby
class ArticlesController < ApplicationController

  http_basic_authenticate_with name: "dhh", password: "secret", except: [:index, :show]

  def index
    @articles = Article.all
  end

  # snippet for brevity
```

També volem permetre que només els usuaris autenticats esborrin comentaris, de manera que a `CommentsController`(`app/controllers/comments_controller.rb`) escriurem:

```ruby
class CommentsController < ApplicationController

  http_basic_authenticate_with name: "dhh", password: "secret", only: :destroy

  def create
    @article = Article.find(params[:article_id])
    # ...
  end

  # snippet for brevity
```

Ara, si proveu de crear un article nou, us mostrarà un pop-up d'autenticació HTTP bàsic:

![Repte bàsic d'autenticació HTTP](https://guides.rubyonrails.org/v7.0/images/getting_started/challenge.png "Repte bàsic d'autenticació HTTP")

Hi ha altres mètodes d'autenticació disponibles per a les aplicacions de Rails. Dos add-ons d'autenticació populars per a Rails són el motor [Devise](https://github.com/plataformatec/devise) de Rails  i la gema [Authlogic](https://github.com/binarylogic/authlogic), juntament amb una sèrie d'altres mètodes.

<br>



### 11.2. Altres consideracions de seguretat

La seguretat, especialment en aplicacions web, és una àrea àmplia i detallada. La seguretat a la vostra aplicació de Rails es tracta amb més profunditat a la [Guia de seguretat de Ruby on Rails](https://guides.rubyonrails.org/v7.0/security.html).

<br>



## 12. Quin és el següent pas?

Ara que heu creat la vostra primera aplicació en Rails, no dubteu a actualitzar-la i experimentar pel vostre compte.

Recordeu que no heu de fer-ho tot sense ajuda. Si necessiteu ajuda per iniciar-vos en Rails, no dubteu a consultar aquests recursos d'assistència:

* Les [guies de Ruby on Rails](https://guides.rubyonrails.org/v7.0/index.html).
* La [llista de correu de Ruby on Rails](https://discuss.rubyonrails.org/c/rubyonrails-talk).
* El canal [#rubyonrails](irc://irc.freenode.net/#rubyonrails) a irc.freenode.net.

<br>



## 13. Configuracions a tenir en compte

La manera més senzilla de treballar amb Rails és emmagatzemar totes les dades externes en UTF-8. Si no ho feu, les llibreries de Ruby i Rails sovint podran convertir les vostres dades natives a UTF-8, però això no sempre funciona de manera fiable, així que us recomanem que us assegureu que totes les dades externes estiguin en UTF-8.

Si us heu equivocat en aquest aspecte, el símptoma més comú és un diamant negre amb un signe d'interrogació a l'interior que apareix al navegador. Un altre símptoma comú és que apareixen caràcters com "Ã¼" en lloc de "ü". Rails utilitza una sèrie de mètodes interns per mitigar les causes comuns d'aquests problemes, i normalment ho pot detectar i corregir automàticament. Tanmateix, si teniu dades externes que no s'emmagatzemen en UTF-8, ocasionalment pot provocar aquest tipus de problemes on Rails no pot detectar i corregir automàticament.

Dues fonts molt habituals que no utilitzen UTF-8 són:

* El vostre editor de text: la majoria dels editors de text (com ara _TextMate_) desen fitxers en UTF-8 per defecte. Si el vostre editor de text no ho fa, pot ser que els caràcters especials que introduïu a les vostres plantilles (com ara é) apareguin com un diamant amb un signe d'interrogació al navegador. Això també s'aplica als vostres fitxers de traducció i18n. La majoria dels editors que encara no utilitzen UTF-8 per defecte (com ara algunes versions de _Dreamweaver_) ofereixen una manera de canviar el valor predeterminat a UTF-8. Fes-ho.
* La vostra base de dades: Rails per defecte converteix les dades de la vostra base de dades a UTF-8. Tanmateix, si la vostra base de dades no utilitza UTF-8 internament, és possible que no pugui emmagatzemar tots els caràcters que introdueixen els vostres usuaris. Per exemple, si la vostra base de dades utilitza Llatí-1 internament i l'usuari introdueix un caràcter rus, hebreu o japonès, les dades es perdran per sempre un cop entri a la base de dades. Si és possible, utilitzeu UTF-8 com a codificació interna de la vostra base de dades.

<br>



## Feedback

Us animem a ajudar a millorar la qualitat d'aquesta guia.

Si us plau, contribuïu si veieu alguna errada ortogràfica o errors de contingut. Per començar, podeu llegir la secció d'[aportacions a la documentació](https://edgeguides.rubyonrails.org/contributing_to_ruby_on_rails.html#contributing-to-the-rails-documentation).

També podeu trobar contingut incomplet o coses que no estiguin actualitzades. Si us plau, afegiu a la documentació la informació que faci falta. Assegureu-vos de comprovar primer les [guies Edge](https://edgeguides.rubyonrails.org/) per verificar si els problemes ja estan solucionats o no a la branca principal. Consulteu les directrius de les [guies de Ruby on Rails](https://guides.rubyonrails.org/v7.0/ruby_on_rails_guides_guidelines.html) per a l'estil i les convencions.

Si per qualsevol motiu trobeu alguna cosa que es pugui arreglar però no podeu modificar-ho vosaltres mateixos, [obriu una _issue_](https://github.com/rails/rails/issues).

I, finalment, però no menys important, qualsevol mena de discussió sobre la documentació de Ruby on Rails és molt benvinguda a la [llista de correu _rubyonrails-docs_](https://discuss.rubyonrails.org/c/rubyonrails-docs).
