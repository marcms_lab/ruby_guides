#!/bin/bash

VM_IP="$(hostname -I)"
VM_PORT="3030"
PROJECT_NAME="blog"
PROJECTS_PATH="/vagrant/0.start_here/0.1.getting_started_with_rails"
EXEC_COMANDS=false

# -------------------- #

# String formatters
tty_escape() { printf "\033[%sm" "$1"; }
tty_base()   { tty_escape "1;$1"; }
tty_url="$(tty_base 4)"
tty_red="$(tty_base 31)"
tty_gre="$(tty_base 32)"
tty_yel="$(tty_base 33)"
tty_blu="$(tty_base 34)"
tty_pin="$(tty_base 35)"
tty_cya="$(tty_base 36)"
tty_bol="$(tty_base 39)"
tty_con="$(tty_base 40)"
tty_reset="$(tty_escape 0)"
tty_quote="$(tty_escape 2)"
tty_rcon="${tty_reset}${tty_con}"
tty_rquote="${tty_reset}${tty_quote}"

# Message formatters
abort()    { printf "${tty_red}[Error]${tty_reset}: %s\n\n" "$@" 1>&2; exit 1; }
Warning()  { printf "${tty_red}[Warning]${tty_reset}: %s\n\n" "$@"; }
info()     { printf "${tty_gre}[Info]${tty_reset}: %s\n\n" "$@"; }
title()    { printf "${tty_gre}%s${tty_reset}\n\n" "$@"; }
quote()    { printf "${tty_quote}${tty_blu}<%s>${tty_rquote} %s${tty_reset}\n\n" "$@"; continue; }
terminal() { printf "${tty_con} %s ${tty_reset}\n\n" "$@"; continue; }
show()     { printf "%s\n\n" "$@"; continue; }
exec()     { sleep 1s; printf "${tty_blu}[Exec]${tty_reset}: %s\n" "$@"; }
end_exec() { sleep 1s; printf "${tty_blu}[Exec]${tty_reset}: %s\n\n" "Done!"; }
continue() { read -n 1 -s -r -p ""; }
continue_message() { read -n 1 -s -r -p "${tty_quote}Press any key to continue...${tty_reset}"; show ""; }

# -------------------- #

# Check rails installation
[[ -x "$(command -v rails)" ]] || abort "Rails installation not found."

# -------------------- #

# Initialize project

# if [[ ! -d "$PROJECTS_PATH/$PROJECT_NAME" ]]; then
#   info "Initializing project $PROJECT_NAME..."
#   cd "$PROJECTS_PATH"
#   rails new "$PROJECT_NAME" || abort "Can't create the project $PROJECT_NAME."
# fi

# -------------------- #

echo ""
title "${tty_gre}4.1. Starting up the Web Server${tty_reset}"

show "You actually have a functional Rails application already.
To see it, you need to start a web server on your development machine.
You can do this by running the following command in the ${tty_con}blog${tty_reset} directory:"

terminal "\$ cd ${PROJECT_NAME}/"
terminal "\$ bin/rails server"

quote "!" "If you are using Windows, you have to pass the scripts under the ${tty_con}bin${tty_rquote} folder directly to the Ruby interpreter e.g. ${tty_con}ruby bin\rails server${tty_rquote}."

quote "!" "JavaScript asset compression requires you have a JavaScript runtime available on your system, in the absence of a runtime you will see an ${tty_con}execjs${tty_rquote} error during asset compression. Usually macOS and Windows come with a JavaScript runtime installed. ${tty_con}therubyrhino${tty_rquote} is the recommended runtime for JRuby users and is added by default to the ${tty_con}Gemfile${tty_rquote} in apps generated under JRuby. You can investigate all the supported runtimes at ExecJS:${tty_url}https://github.com/rails/execjs#readme${tty_rquote}."

# Start puma server
if ${EXEC_COMANDS}; then
  exec "Starting server..."
  nohup ${PROJECTS_PATH}/${PROJECT_NAME}/bin/rails server -u puma -b ${VM_IP} 2>/dev/null &
  end_exec
  sleep 1s
fi



show "This will start up Puma, a web server distributed with Rails by default. To see your application in action, open a browser window and navigate to:"

terminal "${tty_url}http://localhost:${VM_PORT}${tty_rcon}"

show "You should see the Rails default information page."

# -------------------- #

# Stop puma server
if ${EXEC_COMANDS}; then
  exec "Stopping server..."
  ps -ef | grep 'puma' | grep -v 'grep' | awk '{ print $2 }' | xargs -r kill -9 && wait 2>/dev/null
  end_exec
  sleep 1s
fi

# -------------------- #

echo "Bye, bye!"
echo ""
