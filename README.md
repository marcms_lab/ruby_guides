
Ruby on Rails Guides (v7.0.2.3)
====================

@marcms_lab unofficial Ruby on Rails Guides
--------------------

These are the new guides for Rails 7.0 based on v7.0.2.3.
These guides are designed to make you immediately productive with Rails, and to help you understand how all of the pieces fit together.

You can check these guides in one of this languages:

* [English](https://guides.rubyonrails.org/v7.0/) (Official)

* [Català](README_ca.md)
